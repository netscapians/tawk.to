//
//  UserDetails.swift
//  twak.io
//
//  Created by Gaurav Sethi on 07/02/22.
//
import UIKit
import Foundation

class UserDetails : UIViewController {
    
    @IBOutlet weak var lblFollowing: UILabel!
    @IBOutlet weak var lblFollowers: UILabel!
    @IBOutlet weak var txtNotes: UITextView!
    @IBOutlet weak var lblBlog: UILabel!
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    
    var userName = String()
    var userDetails = UserDetailsViewModel()
    
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }

    //------------------------------------------------------
    
    //MARK: load image
    
    func loadImage(urlString : String) {
        let imgurl = URL(string: urlString) ?? URL(fileURLWithPath: "")
        let session = URLSession(configuration: .default)

        let downloadPicTask = session.dataTask(with: imgurl) { (data, response, error) in
            if let e = error {
                print("Error downloading cat picture: \(e)")
            } else {

                if let res = response as? HTTPURLResponse {
                    print("Downloaded cat picture with response code \(res.statusCode)")
                    if let imageData = data {
                        DispatchQueue.main.async {
                            self.imgProfile.image = UIImage(data: imageData)
                        }
                    } else {
                        print("Couldn't get image: Image is nil")
                    }
                } else {
                    print("Couldn't get response code for some reason")
                }
            }
        }
        downloadPicTask.resume()
    }
    
    //------------------------------------------------------
    
    //MARK: Action
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSave(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //------------------------------------------------------
    
    //MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userDetails.getUserDetails(userName: userName)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.lblName.text = self.userDetails.userDetailsArray.login
            self.lblUserName.text = "Name : \(self.userDetails.userDetailsArray.login ?? "")"
            self.lblCompanyName.text = "Company Name : \(self.userDetails.userDetailsArray.company ?? "")"
            self.lblBlog.text = "Blog : \(self.userDetails.userDetailsArray.blog ?? "")"
            self.loadImage(urlString: self.userDetails.userDetailsArray.avatarURL ?? "")
            self.lblFollowers.text = "Followers :\(self.userDetails.userDetailsArray.followers ?? Int())"
            self.lblFollowing.text = "Followers :\(self.userDetails.userDetailsArray.following ?? Int())"
        }
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if InternetConnectionManager.isConnectedToNetwork(){
            
        }else {
            self.navigationController?.view.makeToast("Please check your internet connection.", duration: 3.0, position: .bottom)
        }
    }
    
    //------------------------------------------------------
}
