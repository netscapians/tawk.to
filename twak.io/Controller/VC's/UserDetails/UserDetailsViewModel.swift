//
//  UserDetailsViewModel.swift
//  twak.io
//
//  Created by Gaurav Sethi on 08/02/22.
//

import CoreData
import Foundation

class UserDetailsViewModel {
    
    weak var vc: UserDetails?
    var userDetailsArray = UserDetailsModel()
    
    //------------------------------------------------------
    
    //MARK: Service call with URL Session
    
    func getUserDetails(userName : String )  {
        let serviceUrl = "https://api.github.com/users/" + "\(userName)"
        if InternetConnectionManager.isConnectedToNetwork(){
            URLSession.shared.dataTask(with: URL(string: serviceUrl)!) { data, response, error in
                if error == nil {
                    if let data = data {
                        do{
                            let responseString = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as? [String: Any]
                            let data : UserDetailsModel = UserDetailsModel(login: responseString?["login"] as? String , id: responseString?["id"] as? Int, nodeID: responseString?["node_id"] as? String, avatarURL: responseString?["avatar_url"] as? String, gravatarID: responseString?["gravatar_id"] as? String, url: responseString?["url"] as? String, htmlURL: responseString?["html_url"] as? String, followersURL: responseString?["followers_url"] as? String, followingURL: responseString?["following_url"] as? String, gistsURL: responseString?["gists_url"] as? String, starredURL: responseString?["starred_url"] as? String, subscriptionsURL: responseString?["subscriptions_url"] as? String, organizationsURL: responseString?["organizations_url"] as? String, reposURL: responseString?["repos_url"] as? String, eventsURL: responseString?["events_url"] as? String, receivedEventsURL: responseString?["received_events_url"] as? String, type: responseString?["type"] as? String, siteAdmin: responseString?["site_admin"] as? Bool, name: responseString?["name"] as? String, company: responseString?["company"] as? String, blog: responseString?["blog"] as? String, location: responseString?["location"] as? String, email: responseString?["email"] as? JSONNull, hireable: responseString?["hireable"] as? Any as? JSONNull, bio: responseString?["bio"] as? Any as? JSONNull, twitterUsername: responseString?["twitter_username"] as? Any as? JSONNull, publicRepos: responseString?["public_repos"] as? Int, publicGists: responseString?["public_gists"] as? Int, followers: responseString?["followers"] as? Int, following: responseString?["following"] as? Int, createdAt: responseString?["created_at"] as? Date, updatedAt: responseString?["updated_at"] as? Date)
                            self.userDetailsArray = data
                            print(self.userDetailsArray)
                        } catch let err{
                            print(err.localizedDescription)
                        }
                    }
                } else {
                    print(error?.localizedDescription ?? "")
                }
            }.resume()
        }else{
            print("Not Connected")
        }
    }
}

