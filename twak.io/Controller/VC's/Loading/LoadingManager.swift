//
//  LoadingManager.swift
//  twak.io
//
//  Created by Gaurav Sethi on 04/02/22.
//

import UIKit
import Foundation

class LoadingManager: NSObject {
    
    var controller: LoadingIndicatorVC?
    var alertController: UIAlertController?
    
    //------------------------------------------------------
    
    //MARK: Shared
    
    static let shared = LoadingManager()

    //------------------------------------------------------
    
    //MARK: Show
    
    func showLoading(isOpacity: Bool = true) {
        
        if controller == nil || controller?.presentingViewController != nil {
            controller?.modalPresentationStyle = .overFullScreen
            controller?.isSetSplashScreenOpacity = isOpacity
        }
    }
        
    //------------------------------------------------------
    
    //MARK: Hide
    
    func hideLoading() {
        controller?.dismiss(animated: false, completion: nil)
        controller = nil
    }
    
    //------------------------------------------------------
}
