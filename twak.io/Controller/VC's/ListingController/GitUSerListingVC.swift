//
//  GitUSerListingVC.swift
//  twak.io
//
//  Created by Gaurav Sethi on 04/02/22.
//
import UIKit
import Foundation

class GitUSerListingVC : UIViewController , UITableViewDelegate , UITableViewDataSource, UISearchBarDelegate {
    
    //------------------------------------------------------
    
    //MARK: Outlets
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tblUserListing: UITableView!
    
    //------------------------------------------------------
    
    //MARK: Varible decleration
    
    var userDetails = UserViewModel()
    var activityIndicator: LoadMoreActivityIndicator!
    var isSearch = Bool()
    var debounce: SCDebouncer?
    
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //------------------------------------------------------
    
    //MARK: Custome
    
    func setup() {
        let identifier = String(describing: GitUserListCell.self)
        let nibCell = UINib(nibName: identifier, bundle: Bundle.main)
        tblUserListing.register(nibCell, forCellReuseIdentifier: identifier)
        
        view.addSubview(tblUserListing)
        tblUserListing.translatesAutoresizingMaskIntoConstraints = false
        tblUserListing.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        tblUserListing.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 32, right: 0)
        tblUserListing.tableFooterView = UIView()
        activityIndicator = LoadMoreActivityIndicator(scrollView: tblUserListing, spacingFromLastCell: 32, spacingFromLastCellWhenLoadMoreActionStart: 60)
        searchBar.delegate = self
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //MARK: UISearchbar delegate
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        isSearch = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        isSearch = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        self.userDetails.searchDataArray.removeAll()
        isSearch = false
        if isSearch == false {
            self.tblUserListing.reloadData()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        isSearch = false
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if isSearch == true {
            for obj in self.userDetails.userArray {
                if  obj.login?.capitalized.contains(searchText.capitalized) == true{
                    self.userDetails.searchDataArray.append(obj)
                }
            }
            userDetails.searchDataArray = userDetails.searchDataArray.removingDuplicates()
            print(userDetails.userArray.count)
            tblUserListing.reloadData()
        }
        tblUserListing.reloadData()
    }
    
    //------------------------------------------------------
    
    //MARK: Tabel view delegate datasourse
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearch == true {
            return userDetails.searchDataArray.count
        } else {
            return userDetails.userArray.count

        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GitUserListCell", for: indexPath) as! GitUserListCell
        cell.setupData(userData: userDetails.userArray[indexPath.row])
        cell.imgProfile.makeRounded()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "UserDetails") as! UserDetails
        vc.userName = userDetails.userArray[indexPath.row].login ?? ""
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if InternetConnectionManager.isConnectedToNetwork(){
            let lastElement = userDetails.userArray.count
            if lastElement == userDetails.userArray.count {
                self.activityIndicator.start {
                    self.userDetails.getUserData(since: "\(self.userDetails.userArray[indexPath.row].id ?? 0)")
                    self.activityIndicator.stop()
                }
            } else {
                DispatchQueue.main.async { [weak self] in
                    self?.activityIndicator.stop()
                }
            }
        }else{
            
        }
    }
    
    //------------------------------------------------------
    
    //MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userDetails.getUserData(since: "0")
        userDetails.vc = self
        setup()
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if InternetConnectionManager.isConnectedToNetwork(){
            
        }else {
            self.navigationController?.view.makeToast("Please check your internet connection.", duration: 3.0, position: .bottom)
        }
    }
    
    //------------------------------------------------------
}
