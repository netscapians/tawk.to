//
//  GitlistingUserViewModel.swift
//  twak.io
//
//  Created by Gaurav Sethi on 04/02/22.
//

import CoreData
import Foundation

class UserViewModel {
    
    weak var vc: GitUSerListingVC?
    var userArray = [UserModel]()
    var searchDataArray = [UserModel]()
    
    //------------------------------------------------------
    
    //MARK: Service call with URL Session
    
    func getUserData(since : String)  {
        let serviceUrl = "https://api.github.com/users?since=" + "\(since)"
        if InternetConnectionManager.isConnectedToNetwork(){
            URLSession.shared.dataTask(with: URL(string: serviceUrl)!) { data, response, error in
                if error == nil {
                    if let data = data {
                        do{
                            let userResponse = try JSONDecoder().decode([UserModel].self, from: data)
                            print(userResponse)
                            self.userArray.append(contentsOf: userResponse)
//                            self.userArray = self.userArray.removingDuplicates()
                            CoreDatabase.shared.saveDataInCoreData(dataForSave: userResponse)
                            DispatchQueue.main.async {
                                self.vc?.tblUserListing.reloadData()
                            }
                            
                        } catch let err{
                            print(err.localizedDescription)
                        }
                    }
                
                } else {
                    print(error?.localizedDescription ?? "")
                }
            }.resume()
        }else{
            print("Not Connected")
            CoreDatabase.shared.loadSaveData { data in
                self.userArray = data
                DispatchQueue.main.async {
                    self.vc?.tblUserListing.reloadData()
                }
            }
        }
    }
}
