//
//  GitUserListCell.swift
//  twak.io
//
//  Created by Gaurav Sethi on 04/02/22.
//

import UIKit

class GitUserListCell: UITableViewCell {
    
    //------------------------------------------------------
    
    //MARK: Outlets
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblDetails: UILabel!
    @IBOutlet weak var lblUsername: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    //------------------------------------------------------
    
    //MARK: Setup data
    
    func setupData(userData : UserModel) {
        lblUsername.text = userData.login
        lblDetails.text = userData.htmlURL
        loadImage(urlString: userData.avatarURL ?? "")
    }
    
    //------------------------------------------------------
    
    //MARK: load image
    
    func loadImage(urlString : String) {
        let imgurl = URL(string: urlString) ?? URL(fileURLWithPath: "")
        let session = URLSession(configuration: .default)

        let downloadPicTask = session.dataTask(with: imgurl) { (data, response, error) in
            if let e = error {
                print("Error downloading cat picture: \(e)")
            } else {

                if let res = response as? HTTPURLResponse {
                    print("Downloaded cat picture with response code \(res.statusCode)")
                    if let imageData = data {
                        DispatchQueue.main.async {
                            self.imgProfile.image = UIImage(data: imageData)
                        }
                    } else {
                        print("Couldn't get image: Image is nil")
                    }
                } else {
                    print("Couldn't get response code for some reason")
                }
            }
        }
        downloadPicTask.resume()
    }
}

