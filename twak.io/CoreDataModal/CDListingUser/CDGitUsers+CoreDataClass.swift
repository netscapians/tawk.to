//
//  CDGitUsers+CoreDataClass.swift
//  twak.io
//
//  Created by Gaurav Sethi on 04/02/22.
//
//

import UIKit
import CoreData
import Foundation

@objc(CDGitUsers)
public class CDGitUsers: NSManagedObject {
    
}

class CoreDatabase : NSObject {
    static let shared = CoreDatabase()
    
    func loadSaveData(completionBlock : @escaping ((_ data : [UserModel]) -> Void))  {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CDGitUsers")
        
        do {
            let results = try context.fetch(fetchRequest)
            let obtainedResults = results as! [NSManagedObject]
            if obtainedResults.count > 0{
                var userData : [UserModel] = []
                for data in obtainedResults {
                    let login = data.value(forKey: AppConstants.login) as? String ?? ""
                    let id = data.value(forKey: AppConstants.id) as? Int ?? 0
                    let node_id = data.value(forKey: AppConstants.node_id) as? String ?? ""
                    let avatare_url = data.value(forKey: AppConstants.avatare_url) as? String ?? ""
                    let gravatar_id = data.value(forKey: AppConstants.gravatar_id) as? String ?? ""
                    let html_url = data.value(forKey: AppConstants.html_url) as? String ?? ""
                    let followers_url = data.value(forKey: AppConstants.followers_url) as? String ?? ""
                    let following_url = data.value(forKey: AppConstants.following_url) as? String ?? ""
                    let gist_url = data.value(forKey: AppConstants.gist_url) as? String ?? ""
                    let starred_url = data.value(forKey: AppConstants.starred_url) as? String ?? ""
                    let subscription_url = data.value(forKey: AppConstants.subscription_url) as? String ?? ""
                    let organization_url = data.value(forKey: AppConstants.organization_url) as? String ?? ""
                    let repos_url = data.value(forKey: AppConstants.repos_url) as? String ?? ""
                    let events_url = data.value(forKey: AppConstants.events_url) as? String ?? ""
                    let recieved_events_url = data.value(forKey: AppConstants.recieved_events_url) as? String ?? ""
                    let type = data.value(forKey: AppConstants.type) as? String ?? ""
                    let site_admin = data.value(forKey: AppConstants.site_admin) as? Bool ?? false

                    let ourData : UserModel = UserModel(login: login, id: id, nodeID: node_id, avatarURL: avatare_url, gravatarID: gravatar_id, url: "", htmlURL: html_url, followersURL: followers_url, followingURL: following_url, gistsURL: gist_url, starredURL: starred_url, subscriptionsURL: subscription_url, organizationsURL: organization_url, reposURL: repos_url, eventsURL: events_url, receivedEventsURL: recieved_events_url, type: type, siteAdmin: site_admin)
                    
                    userData.append(ourData)
                }
                completionBlock(userData)
            }
        } catch {
            print("Error")
        }
    }
    
    func saveDataInCoreData(dataForSave : [UserModel]) {
        
        DispatchQueue.main.async {
            
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
            
            let managedContext = appDelegate.persistentContainer.viewContext
            
            for data in dataForSave{
                let entity = NSEntityDescription.entity(forEntityName: "CDGitUsers",in: managedContext)!
                
                let item = NSManagedObject(entity: entity,insertInto: managedContext)
                let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CDGitUsers")
                fetchRequest.predicate = NSPredicate(format: "id = %d", data.id!)
                
                let res = try! managedContext.fetch(fetchRequest)
                
                if res.count == 0{
                    item.setValue(data.id, forKey: AppConstants.id)
                    item.setValue(data.login, forKey: AppConstants.login)
                    item.setValue(data.nodeID, forKey: AppConstants.node_id)
                    item.setValue(data.avatarURL, forKey: AppConstants.avatare_url)
                    item.setValue(data.gravatarID, forKey: AppConstants.gravatar_id)
                    item.setValue(data.htmlURL, forKey: AppConstants.html_url)
                    item.setValue(data.followersURL, forKey: AppConstants.followers_url)
                    item.setValue(data.followingURL, forKey: AppConstants.following_url)
                    item.setValue(data.gistsURL, forKey: AppConstants.gist_url)
                    item.setValue(data.starredURL, forKey: AppConstants.starred_url)
                    item.setValue(data.subscriptionsURL, forKey: AppConstants.subscription_url)
                    item.setValue(data.organizationsURL, forKey: AppConstants.organization_url)
                    item.setValue(data.reposURL, forKey: AppConstants.repos_url)
                    item.setValue(data.eventsURL, forKey: AppConstants.events_url)
                    item.setValue(data.receivedEventsURL, forKey: AppConstants.recieved_events_url)
                    item.setValue(data.type, forKey: AppConstants.type)
                    item.setValue(data.siteAdmin, forKey: AppConstants.site_admin)
                }
            }
            
            do {
                try managedContext.save()
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
        }
    }
}

