//
//  CDGitUsers+CoreDataProperties.swift
//  twak.io
//
//  Created by Gaurav Sethi on 04/02/22.
//
//

import Foundation
import CoreData


extension CDGitUsers {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CDGitUsers> {
        return NSFetchRequest<CDGitUsers>(entityName: "CDGitUsers")
    }

    @NSManaged public var login: String?
    @NSManaged public var id: Int16
    @NSManaged public var node_id: String?
    @NSManaged public var avatare_url: String?
    @NSManaged public var gravatar_id: String?
    @NSManaged public var url: String?
    @NSManaged public var html_url: String?
    @NSManaged public var followers_url: String?
    @NSManaged public var following_url: String?
    @NSManaged public var gist_url: String?
    @NSManaged public var starred_url: String?
    @NSManaged public var subscription_url: String?
    @NSManaged public var organization_url: String?
    @NSManaged public var repos_url: String?
    @NSManaged public var events_url: String?
    @NSManaged public var recieved_events_url: String?
    @NSManaged public var type: String?
    @NSManaged public var site_admin: String?

}

extension CDGitUsers : Identifiable {

}
