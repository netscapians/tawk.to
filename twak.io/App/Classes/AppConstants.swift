//
//  AppConstants.swift
//  twak.io
//
//  Created by Netscape Labs on 05/02/22.
//

import Foundation

struct AppConstants {
    
    static let login = "login"
    static let id = "id"
    static let node_id = "node_id"
    static let avatare_url = "avatare_url"
    static let gravatar_id = "gravatar_id"
    static let html_url = "html_url"
    static let followers_url = "followers_url"
    static let following_url = "following_url"
    static let gist_url = "gist_url"
    static let starred_url = "starred_url"
    static let subscription_url = "subscription_url"
    static let organization_url = "organization_url"
    static let repos_url = "repos_url"
    static let events_url = "events_url"
    static let recieved_events_url = "recieved_events_url"
    static let type = "type"
    static let site_admin = "site_admin"
    
    static let name = "name"
    static let followers = "followers"
    static let following = "following"
    static let company = "company"
    static let blog = "blog"
    static let email = "email"
    static let hireable = "hireable"
    static let bio = "bio"
    static let twitter_username = "twitter_username"
    
}

